options 
{
    LOOKAHEAD=1;
}
   
PARSER_BEGIN(Jmm)

import java.io.*;
   
public class Jmm{

    public static void main(String args[]) throws ParseException {
        if (args.length == 0){
            System.out.println("Not enough arguments");
            System.exit(1);
        }
        InputStream fileStream = null;
        try{
            System.out.println(args[0]);
            fileStream = new FileInputStream(args[0]);
        }
        catch (FileNotFoundException e) {
            System.out.println(e);
            System.exit(1);
        }

        Jmm parser = new Jmm(fileStream);
        SimpleNode root = parser.program();

        //Debug
        System.out.println("Completed without errors");
    }
}

PARSER_END(Jmm)
   
SKIP :{
   " " | "\r" | "\t" | "\n"
   | <"//" (~["\n","\r"])* ("\n" | "\r" | "\r\n")>
}
   
TOKEN:{
    < CLASS: "class" >
    | < EXTENDS: "extends" >
    | < PUBLIC: "public" >
    | < STATIC: "static" >
    | < RETURN: "return" >
    | < VOID: "void" >
    | < MAIN: "main" >
    | < STRING: "string" >
    | < INT: "int" >
    | < BOOL: "boolean" >
    | < IF: "if" >
    | < ELSE: "else" >
    | < IMPORT: "import" >
    | < WHILE: "while" >
    | < LENGTH: "length" >
    | < TRUE: "true" >
    | < FALSE: "false" >
    | < THIS: "this" >
    | < NEW: "new" >
    | < NOT: "!" >
    | < COMMA: "," >
    | < SEMICOLON: ";" >
    | < LEFTB: "{" >
    | < RIGHTB: "}" >
    | < LEFTSB: "[" >
    | < RIGHTSB: "]" >
    | < LEFTCB: "(" >
    | < RIGHTCB: ")" >
    | < DOT: "." >
    | < AND: "&&" >
    | < ASSIGN: "=" >
    | < LESSTHAN: "<" >
    | < PLUS: "+" >
    | < MINUS: "-" >
    | < TIMES: "*" >
    | < DIVIDED: "/" >
    | < INTEGERLITERAL: 
        ( ((["+", "-"])?(["0"-"9"])+)
        |("0x"(["0"-"9","a"-"f","A"-"F"])+)
        |("0b"(["0","1"])+))>
    | < IDENTIFIER: ["A"-"Z","a"-"z","$","_"](["0"-"9","A"-"Z","a"-"z","_","$"])*>
}


SimpleNode program() : {}
{
    (ImportDeclaration())*
    ClassDeclaration()
    {return jjtThis;}
}

/** Import **/
void ImportDeclaration() : {}
{
    <IDENTIFIER>
}

/** Class **/

void ClassDeclaration() : {}
{
    // Header
   <CLASS> <IDENTIFIER> (Extends())?
   
   //Body
   <LEFTB>
   (VarDeclaration())*
   (MainDeclaration())?
   (MethodDeclaration())*
   <RIGHTB>
}

void Extends() : {}
{
    <EXTENDS> <IDENTIFIER>
}

/** Variables **/

void ArrayType() : {}
{
   <LEFTSB> <RIGHTSB>
}

void Type() : {}
{
   (<INT> (ArrayType())?) | <BOOL> | <IDENTIFIER>
}

void VarDeclaration() : {}
{
    Type() <IDENTIFIER> <SEMICOLON>
}

/** Expressions **/

void NewExpression() : {}
{
    <NEW> 
    
    // New Int Array
    (<INT> <LEFTSB> Expression() <RIGHTSB>)

    // New Object
    |
    (<IDENTIFIER> <LEFTCB> <RIGHTCB>)
}

void Expression() : {}
{
    //Evaluation
    //(Expression() (<AND> | <LESSTHAN> | <PLUS> | <MINUS> | <TIMES> | <DIVIDED>) Expression())
    
    //Array
    //| 
    //(Expression() <LEFTSB> Expression() <RIGHTSB>)

    //Length
    //|
    //(Expression() <DOT> <LENGTH>)

    //Function
    //|
    //(Expression() <DOT> <IDENTIFIER> <LEFTCB> (Expression() (<COMMA> Expression())* )? <RIGHTCB>)

    //Integer value
    //|
    <INTEGERLITERAL> 

    //True value
    |
    <TRUE>

    //True value
    |
    <FALSE>

    //Var
    |
    <IDENTIFIER>

    //Self Reference
    |
    <THIS>

    // New Expression
    |
    NewExpression()

    //Negation
    |
    (<NOT> Expression())

    //Bracketed Expression
    |
    (<LEFTCB> Expression() <RIGHTCB>)
}

/** Statements **/

void AssignementStatement() : {}
{
    <IDENTIFIER> 

    //Value Assignment
    (<ASSIGN> Expression() <SEMICOLON>) 

    //Array Value Assignment Statement
    |
    (<LEFTSB> Expression() <RIGHTCB> <ASSIGN> Expression() <SEMICOLON>)
}

void Statement() : {}
{
    //Inner Scope
    (<LEFTB> (Statement())* <RIGHTB>)

    //If Statement
    |
    (<IF> <LEFTCB> Expression() <RIGHTB>
    Statement()
    <ELSE> Statement())

    //While Statement
    |
    (<WHILE> <LEFTCB> Expression() <RIGHTCB>
    Statement())

    //Assignment Statement
    |
    AssignementStatement()
}

/** Methods **/

void Arguments() : {}
{
   Type() <IDENTIFIER> 
}

void MethodDeclaration() : {}
{
    // Method Type and Name
    <PUBLIC> Type() <IDENTIFIER> 
    
    //Method Arguments
    <LEFTCB>
    (Arguments() (<COMMA> Arguments())*)?
    <RIGHTB>
    
    //Body
    <LEFTB>
    (VarDeclaration())*
    (Statement())*
    <RETURN> Expression() <SEMICOLON>
    <RIGHTB>
}



void MainDeclaration() : {}
{
    // Main Header
    <PUBLIC> <STATIC> <VOID> <MAIN> <LEFTCB> <STRING> ArrayType() <IDENTIFIER> <RIGHTCB>
    
    //Body
    <LEFTB>
    (VarDeclaration())*
    (Statement())*
    <RIGHTB>
}
